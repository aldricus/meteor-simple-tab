/*
    Meteor SimpleTabWidget - Copyright (C) 2015  Aldric T
    aldric.dev@gmail.com

    License:  GNU General Public License

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
(function() {
    'use strict';
    var SimpleTab = function() {

        var tabs = {
            simpleTab: [],
            options: [],
            init: function(id, options) {

                this.simpleTab[id] = {};
                this.simpleTab[id].$tab = $('#' + id);
                this.simpleTab[id].index = 0;

                if (this.simpleTab[id].$tab === null) {
                    throw 'Your ID cannot be found. You must provide a valide element ID';
                }
                if (options === undefined) {
                    this.options[id] = {};
                } else {
                    this.options[id] = options;
                }
                var $this = this;

                // On récupère la première liste qui est utilisée
                // pour les onglets
                var $tabUl = $this.simpleTab[id].$tab.find('ul')
                    .first();
                $tabUl.addClass('ldrc-tab-container');

                //Create the tab
                var $tabList = $tabUl.find('li');
                $tabList.addClass('ldrc-tab');
                $tabList.addClass(function(idx) {
                    return 'index-' + idx;
                });
                console.log($this.options[id]);
                // Active tab number "index";
                addClass($tabList, 'ldrc-tab-active', $this.simpleTab[
                    id].index);
                //attach event
                var onOpenTab = function(e) {
                    $this.simpleTab[id].index = $(this).index();
                    var $previousTab = $this.simpleTab[id].$tab
                        .find('.ldrc-tab-active');

                    $previousTab.removeClass(
                        'ldrc-tab-active');
                    $(this).addClass('ldrc-tab-active');

                    var $toHide = $this.simpleTab[id].$tab.find(
                        '.ldrc-content-active');

                    $toHide.removeClass(
                        'ldrc-content-active');
                    $toHide.addClass('ldrc-content-hidden');

                    var $content = $('#' + $(this).find('a')[
                        0].hash.substr(1));

                    $content.addClass('ldrc-content-active');
                    $content.removeClass(
                        'ldrc-content-hidden');
                    if ($this.options[id].onOpenTab !==
                        undefined) {
                        /*
							//Function called when use click on a tab
							options.onOpenTab: function(panel){

								if(panel.attr('id') == 'monPanel1'){
									panel.html('<div> Hello Panel1 </div>');
								}else if(panel.id == 'monPanel2'){
									panel.html('<div> Hello Panel2 </div>');
								}
							}

						 */
                        console.log($content);
                        $this.options[id].onOpenTab(
                            $content);
                    }

                    // Active first panel of any tab containing under 
                    // this panel
                    var $subParentTab = $content.find(
                        '.ldrc-tab-container').parent();
                    if ($subParentTab.length) {
                        var $subActiveTab = $subParentTab.find(
                            'ul').first().find('li');
                        var subParentId = $subParentTab.attr(
                            'id');
                        console.log($subParentTab);
                        addClass($subActiveTab,
                            'ldrc-tab-active', $this.simpleTab[
                                subParentId].index);
                        var $subActivePanel = $subParentTab
                            .find('.ldrc-content');
                        addClass($subActivePanel,
                            'ldrc-content-active',
                            $this.simpleTab[subParentId]
                            .index);
                        removeClass($subActivePanel,
                            'ldrc-content-hidden',
                            $this.simpleTab[subParentId]
                            .index);
                    }
                }
                $tabList.on('click', onOpenTab);
                $tabList.on('touchend', onOpenTab);

                //Get tab contents
                var $panels = $this.simpleTab[id].$tab.children();
                //Do not hide first tab
                for (var i = $panels.length - 1; i >= 0; i--) {
                    if ($panels[i].tagName == 'DIV') {
                        $panels[i].classList.add(
                            'ldrc-content-hidden');
                        $panels[i].classList.add('ldrc-content');
                    }

                }
                //active first panel
                var $activePanel = $this.simpleTab[id].$tab.find(
                    '.ldrc-content');
                addClass($activePanel, 'ldrc-content-active',
                    $this.simpleTab[id].index);
                removeClass($activePanel, 'ldrc-content-hidden',
                    $this.simpleTab[id].index);

                return this;
            }
        };
        return tabs;
    };
    var addClass = function(object, className, index) {
        object.addClass(function(idx) {
            if (idx === index) {
                return className;
            }
        });
    };
    var removeClass = function(object, className, index) {
        object.removeClass(function(idx) {
            if (idx === index) {
                return className;
            }
        });
    };

    window.SimpleTab = SimpleTab;
})(typeof window === 'undefined' ? this : window);