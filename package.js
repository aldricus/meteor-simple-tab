Package.describe({
  name: 'aldricus:simple-tab',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.0.3.2');
  api.addFiles('client/simple-tab.css','client');
  api.addFiles('client/simple-tab.js','client');
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('aldricus:simple-tab');
  //api.addAssets('tests/aldricus:simple-tab-tests.js');
});
